<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use AppBundle\Entity\deplacement;

class deplacementController extends Controller {
/**
* @Route("/create-deplacement")
*/
public function createAction(Request $request) {

  $deplacement = new deplacement();
  $form = $this->createFormBuilder($deplacement)
   
    ->add('nom', TextType::class)
    ->add('prenom', TextType::class)
    ->add('email', TextType::class)

    ->add('datedepart', DateType::class)
    ->add('dateretour', DateType::class)
    ->add('heuredepart', TimeType::class)
    ->add('heureretour', TimeType::class)
    
->add('moyentransport',ChoiceType::class,array(
                'choices'  => array(
                    'avion' => 'avion',
                    'voiture' => 'voiture',
                    'metro' => 'metro',
                ),
                'expanded' => true,
                'multiple' => false
            ))
->add('idville', ChoiceType::class, [
    'choices' => [
            'Bizerte'=> 'Bizerte',
            'Tunis'=> 'Tunis',
            'Sousse'=> 'Sousse',
            'Sfax'=> 'Sfax',

        ],
])
->add('save', SubmitType::class, array('label' => 'enregistrer'))
->add('annuler', SubmitType::class, array('label' => 'annuler'))
->getForm();


$form->handleRequest($request);

if ($form->isSubmitted()) {
    
    $deplacement = $form->getData();
    
    $em = $this->getDoctrine()->getManager();
    $em->persist($deplacement);
    $em->flush();
    return $this->redirect('/view-deplacement');
    
}

return $this->render(
    'deplacement/edit.html.twig',
    array('form' => $form->createView())
    );


}
/**
 * @Route("/view-deplacement/{id}")
 */
public function viewAction($id) {
    
    $deplacement = $this->getDoctrine()
    ->getRepository('AppBundle:deplacement')
    ->find($id);
    
    if (!$deplacement) {
        throw $this->createNotFoundException(
            'There are no deplacement with the following id: ' . $id
            );
    }
    
    return $this->render(
        'deplacement/view.html.twig',
        array('deplacement' => $deplacement)
        );
    
}
/**
 * @Route("/show-deplacement/")
 */
public function showAction() {
    
    $deplacement = $this->getDoctrine()
    ->getRepository('AppBundle:deplacement')
    ->findAll();
    
    return $this->render(
        'deplacement/show.html.twig',
        array('deplacement' => $deplacement)
        );
    
}
}
?>
