<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="ville")
*/
class ville {

  /**
  * @ORM\Column(type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $idville;

  /**
  * @ORM\Column(type="string", length=100)
  */
  protected $nomville;

  /**
  * @ORM\Column(type="string", length=50)
  */

    /**
     * Get idville.
     *
     * @return int
     */
    public function getIdville()
    {
        return $this->idville;
    }

    /**
     * Set nomville.
     *
     * @param string $nomville
     *
     * @return ville
     */
    public function setNomville($nomville)
    {
        $this->nomville = $nomville;

        return $this;
    }

    /**
     * Get nomville.
     *
     * @return string
     */
    public function getNomville()
    {
        return $this->nomville;
    }
}
