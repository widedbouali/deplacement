<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="deplacement")
*/
class deplacement {

  /**
  * @ORM\Column(type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;

  /**
  * @ORM\Column(type="string", length=100)
  */
  protected $nom;

  /**
  * @ORM\Column(type="string", length=50)
  */
  protected $prenom;

  /**
  * @ORM\Column(type="string", length=1000)
  */
  protected $email;

  /**
  * @ORM\Column(type="date", length=200, nullable=true)
  */
 protected $datedepart;

  /**
  * @ORM\Column(type="date", length=200, nullable=true)
  */
 protected $dateretour;

  /**
  * @ORM\Column(type="time", length=200, nullable=true)
  */
 protected $heuredepart;

  /**
  * @ORM\Column(type="time", length=6, nullable=true)
  */
 protected $heureretour;

  /**
  * @ORM\Column(type="string", length=200, nullable=true)
  */
 protected $moyentransport;

  /**
  * @ORM\Column(type="string", length=200, nullable=true)
  */
 protected $idville;

  /**
  * @ORM\Column(type="string", length=100)
  */

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return deplacement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return deplacement
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return deplacement
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set datedepart.
     *
     * @param string|null $datedepart
     *
     * @return deplacement
     */
    public function setDatedepart($datedepart = null)
    {
        $this->datedepart = $datedepart;

        return $this;
    }

    /**
     * Get datedepart.
     *
     * @return string|null
     */
    public function getDatedepart()
    {
        return $this->datedepart;
    }

    /**
     * Set dateretour.
     *
     * @param \DateTime|null $dateretour
     *
     * @return deplacement
     */
    public function setDateretour($dateretour = null)
    {
        $this->dateretour = $dateretour;

        return $this;
    }

    /**
     * Get dateretour.
     *
     * @return \DateTime|null
     */
    public function getDateretour()
    {
        return $this->dateretour;
    }

    /**
     * Set heuredepart.
     *
     * @param \DateTime|null $heuredepart
     *
     * @return deplacement
     */
    public function setHeuredepart($heuredepart = null)
    {
        $this->heuredepart = $heuredepart;

        return $this;
    }

    /**
     * Get heuredepart.
     *
     * @return \DateTime|null
     */
    public function getHeuredepart()
    {
        return $this->heuredepart;
    }

    /**
     * Set heureretour.
     *
     * @param \DateTime|null $heureretour
     *
     * @return deplacement
     */
    public function setHeureretour($heureretour = null)
    {
        $this->heureretour = $heureretour;

        return $this;
    }

    /**
     * Get heureretour.
     *
     * @return \DateTime|null
     */
    public function getHeureretour()
    {
        return $this->heureretour;
    }

    /**
     * Set moyentransport.
     *
     * @param \DateTime|null $moyentransport
     *
     * @return deplacement
     */
    public function setMoyentransport($moyentransport = null)
    {
        $this->moyentransport = $moyentransport;

        return $this;
    }

    /**
     * Get moyentransport.
     *
     * @return \DateTime|null
     */
    public function getMoyentransport()
    {
        return $this->moyentransport;
    }

    /**
     * Set idville.
     *
     * @param string|null $idville
     *
     * @return deplacement
     */
    public function setIdville($idville = null)
    {
        $this->idville = $idville;

        return $this;
    }

    /**
     * Get idville.
     *
     * @return string|null
     */
    public function getIdville()
    {
        return $this->idville;
    }
}
